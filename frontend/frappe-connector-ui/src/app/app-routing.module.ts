import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListingComponent } from './shared-ui/listing/listing.component';
import { AuthGuard } from './common/guards/auth-guard/auth.guard.service';
import { HomeComponent } from './shared-ui/home/home.component';
import { DashboardComponent } from './shared-ui/dashboard/dashboard.component';
import { FrappeClientComponent } from './frappe-connector-ui/frappe-client/frappe-client.component';
import { RequestLogComponent } from './frappe-connector-ui/request-log/request-log.component';
import { FrappeConnectorSettingsComponent } from './frappe-connector-ui/frappe-connector-settings/frappe-connector-settings.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'dashboard', component: DashboardComponent },

  {
    path: 'list/frappe',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'list/request_log',
    component: ListingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'form/frappe/:id',
    component: FrappeClientComponent,
    canActivateChild: [AuthGuard],
  },
  {
    path: 'form/request_log/:id',
    component: RequestLogComponent,
    canActivateChild: [AuthGuard],
  },
  {
    path: 'settings',
    component: FrappeConnectorSettingsComponent,
    canActivate: [AuthGuard],
  },

  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
