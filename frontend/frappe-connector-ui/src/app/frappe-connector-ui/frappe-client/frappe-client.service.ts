import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../../app/common/services/storage/storage.service';
import { APP_URL } from '../../../app/constants/storage';

export const GET_FRAPPE_CLIENT_ENDPOINT = '/frappe/v1/get/';
export const CREATE_FRAPPE_CLIENT_ENDPOINT = '/frappe/v1/connect_client';
export const UPDATE_FRAPPE_CLIENT_ENDPOINT = '/frappe/v1/update_client/';

@Injectable({
  providedIn: 'root',
})
export class FrappeClientService {
  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) {}

  getClient(uuid: string) {
    const appURL = this.storage.getInfo(APP_URL);
    const requestURL = appURL + GET_FRAPPE_CLIENT_ENDPOINT + uuid;
    return this.http.get<any>(requestURL);
  }

  createClient(
    name: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    authServerURL: string,
    authorizationURL: string,
    revocationURL: string,
    introspectionURL: string,
    scope: string[],
  ) {
    const client = {
      name,
      clientId,
      clientSecret,
      profileURL,
      tokenURL,
      authServerURL,
      authorizationURL,
      revocationURL,
      introspectionURL,
      scope,
    };

    const appURL = this.storage.getInfo(APP_URL);
    const requestUrl = appURL + CREATE_FRAPPE_CLIENT_ENDPOINT;
    return this.http.post(requestUrl, client);
  }

  updateClient(
    uuid: string,
    name: string,
    clientId: string,
    clientSecret: string,
    profileURL: string,
    tokenURL: string,
    authServerURL: string,
    authorizationURL: string,
    revocationURL: string,
    introspectionURL: string,
    scope: string[],
  ) {
    const client = {
      name,
      clientId,
      clientSecret,
      profileURL,
      tokenURL,
      authServerURL,
      authorizationURL,
      revocationURL,
      introspectionURL,
      scope,
    };

    const appURL = this.storage.getInfo(APP_URL);
    const requestUrl = appURL + UPDATE_FRAPPE_CLIENT_ENDPOINT + uuid;
    return this.http.post(requestUrl, client);
  }
}
