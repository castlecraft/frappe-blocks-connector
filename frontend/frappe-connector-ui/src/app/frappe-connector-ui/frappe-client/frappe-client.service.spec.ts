import { TestBed } from '@angular/core/testing';

import { FrappeClientService } from './frappe-client.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StorageService } from '../../../app/common/services/storage/storage.service';

describe('FrappeClientService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: StorageService, useValue: {} }],
    }),
  );

  it('should be created', () => {
    const service: FrappeClientService = TestBed.get(FrappeClientService);
    expect(service).toBeTruthy();
  });
});
