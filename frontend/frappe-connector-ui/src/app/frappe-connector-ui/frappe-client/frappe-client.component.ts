import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { NEW_ID, DURATION } from '../../../app/constants/common';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { FrappeClientService } from './frappe-client.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
} from '../../../app/constants/messages';
import { MatSnackBar } from '@angular/material/snack-bar';

export const FRAPPE_CLIENT_LIST_ROUTE = '/list/frappe';

@Component({
  selector: 'app-frappe-client',
  templateUrl: './frappe-client.component.html',
  styleUrls: ['./frappe-client.component.css'],
})
export class FrappeClientComponent implements OnInit {
  clientName: string;
  clientId: string;
  clientSecret: string;
  profileURL: string;
  tokenURL: string;
  authServerURL: string;
  authorizationURL: string;
  revocationURL: string;
  introspectionURL: string;
  scopeForms: FormArray;
  scope: string[];
  uuid: string;
  model: string;
  hideClientSecret: boolean = true;

  clientForm = new FormGroup({
    clientName: new FormControl(this.clientName),
    clientId: new FormControl(this.clientId),
    clientSecret: new FormControl(this.clientSecret),
    profileURL: new FormControl(this.profileURL),
    tokenURL: new FormControl(this.tokenURL),
    authServerURL: new FormControl(this.authServerURL),
    authorizationURL: new FormControl(this.authorizationURL),
    revocationURL: new FormControl(this.revocationURL),
    introspectionURL: new FormControl(this.introspectionURL),
    scopeForms: new FormArray([]),
  });

  constructor(
    private router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly service: FrappeClientService,
    private snackBar: MatSnackBar,
  ) {
    this.uuid = this.activatedRoute.snapshot.params.id;
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[2];
      });
  }

  ngOnInit() {
    if (this.uuid === NEW_ID) {
      this.uuid = undefined;
      this.clientForm.controls.clientSecret.setValidators([
        Validators.required,
      ]);
    } else {
      this.subscribeFrappeClient(this.uuid);
    }
  }

  createScopesFormGroup(scope?: string): FormGroup {
    return new FormGroup({ scope: new FormControl(scope) });
  }

  addScope(scope?: string) {
    this.scopeForms = this.clientForm.get('scopeForms') as FormArray;
    this.scopeForms.push(this.createScopesFormGroup(scope));
  }

  removeScope(formGroupID: number) {
    this.scopeForms.removeAt(formGroupID);
  }

  subscribeFrappeClient(uuid: string) {
    this.service.getClient(uuid).subscribe({
      next: response => {
        this.clientName = response.name;
        this.clientId = response.clientId;
        this.profileURL = response.profileURL;
        this.tokenURL = response.tokenURL;
        this.authServerURL = response.authServerURL;
        this.authorizationURL = response.authorizationURL;
        this.revocationURL = response.revocationURL;
        this.introspectionURL = response.introspectionURL;
        this.scope = response.scope;

        this.clientForm.controls.clientName.setValue(response.name);
        this.clientForm.controls.clientId.setValue(response.clientId);
        this.clientForm.controls.profileURL.setValue(response.profileURL);
        this.clientForm.controls.tokenURL.setValue(response.tokenURL);
        this.clientForm.controls.authServerURL.setValue(response.authServerURL);
        this.clientForm.controls.authorizationURL.setValue(
          response.authorizationURL,
        );
        this.clientForm.controls.revocationURL.setValue(response.revocationURL);
        this.clientForm.controls.introspectionURL.setValue(
          response.introspectionURL,
        );

        this.scope.forEach(element => {
          this.addScope(element);
        });
      },
      error: error => {},
    });
  }

  createClient() {
    this.service
      .createClient(
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.clientId.value,
        this.clientForm.controls.clientSecret.value,
        this.clientForm.controls.profileURL.value,
        this.clientForm.controls.tokenURL.value,
        this.clientForm.controls.authServerURL.value,
        this.clientForm.controls.authorizationURL.value,
        this.clientForm.controls.revocationURL.value,
        this.clientForm.controls.introspectionURL.value,
        this.getScopes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(FRAPPE_CLIENT_LIST_ROUTE);
        },
        error: error => {
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  updateClient() {
    this.service
      .updateClient(
        this.uuid,
        this.clientForm.controls.clientName.value,
        this.clientForm.controls.clientId.value,
        this.clientForm.controls.clientSecret.value,
        this.clientForm.controls.profileURL.value,
        this.clientForm.controls.tokenURL.value,
        this.clientForm.controls.authServerURL.value,
        this.clientForm.controls.authorizationURL.value,
        this.clientForm.controls.revocationURL.value,
        this.clientForm.controls.introspectionURL.value,
        this.getScopes(),
      )
      .subscribe({
        next: success => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(FRAPPE_CLIENT_LIST_ROUTE);
        },
        error: error => {
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  getScopes(): string[] {
    const scopeFormsGroup = this.clientForm.get('scopeForms') as FormArray;
    const scope: string[] = [];
    for (const control of scopeFormsGroup.controls) {
      scope.push(control.value.scope);
    }
    return scope;
  }
}
