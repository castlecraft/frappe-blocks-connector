import { TestBed } from '@angular/core/testing';

import { FrappeConnectorSettingsService } from './frappe-connector-settings.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../../app/common/testing-helpers';

describe('FrappeConnectorSettingsService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: FrappeConnectorSettingsService = TestBed.get(
      FrappeConnectorSettingsService,
    );
    expect(service).toBeTruthy();
  });
});
