import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { StorageService } from '../../../app/common/services/storage/storage.service';
import { ISSUER_URL, APP_URL } from '../../../app/constants/storage';

export const SETTINGS_GET_ENDPOINT = '/settings/v1/get';
export const SETTINGS_UPDATE_ENDPOINT = '/settings/v1/update';
export const SETTINGS_REGISTER_SERVICE_ENDPOINT =
  '/settings/v1/register_service';

@Injectable({
  providedIn: 'root',
})
export class FrappeConnectorSettingsService {
  headers: HttpHeaders;
  constructor(
    private http: HttpClient,
    private oauthService: OAuthService,
    private storageService: StorageService,
  ) {
    this.headers = new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  getSettings() {
    const requestUrl =
      this.storageService.getInfo(APP_URL) + SETTINGS_GET_ENDPOINT;
    return this.http.get(requestUrl, { headers: this.headers });
  }

  updateSettings(
    appURL: string,
    clientId: string,
    clientSecret: string,
    expireRequestLogInMinutes: number,
    callbackProtocol: string,
  ) {
    const appUrl = this.storageService.getInfo(APP_URL);
    const authServerURL = localStorage.getItem(ISSUER_URL);
    this.headers.append('Content-Type', 'application/json');
    expireRequestLogInMinutes = Number(expireRequestLogInMinutes);

    return this.http.post(
      appUrl + SETTINGS_UPDATE_ENDPOINT,
      {
        authServerURL,
        appURL,
        clientId,
        clientSecret,
        expireRequestLogInMinutes,
        callbackProtocol,
      },
      { headers: this.headers },
    );
  }

  registerService() {
    return this.http.post(SETTINGS_REGISTER_SERVICE_ENDPOINT, {
      headers: this.headers,
    });
  }
}
