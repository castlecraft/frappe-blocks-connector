import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  UPDATE_SUCCESSFUL,
  CLOSE,
  SERVICE_REGISTERED_SUCCESSFULLY,
  UPDATE_ERROR,
} from '../../../app/constants/messages';
import { DURATION } from '../../../app/constants/common';
import { FrappeConnectorSettingsService } from './frappe-connector-settings.service';

@Component({
  selector: 'app-frappe-connector-settings',
  templateUrl: './frappe-connector-settings.component.html',
  styleUrls: ['./frappe-connector-settings.component.css'],
})
export class FrappeConnectorSettingsComponent implements OnInit {
  appURL: string;
  clientId: string;
  clientSecret: string;
  hideClientSecret = true;
  expireRequestLogInMinutes: number;
  callbackProtocol: string;

  settingsForm = new FormGroup({
    appURL: new FormControl(this.appURL),
    clientId: new FormControl(this.clientId),
    clientSecret: new FormControl(this.clientSecret),
    expireRequestLogInMinutes: new FormControl(this.expireRequestLogInMinutes),
    callbackProtocol: new FormControl(this.callbackProtocol),
  });

  constructor(
    private settingsService: FrappeConnectorSettingsService,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    this.settingsService.getSettings().subscribe({
      next: (response: any) => {
        this.appURL = response.appUrl;
        this.populateForm(response);
      },
      error: error => {},
    });
  }

  populateForm(response) {
    this.settingsForm.controls.appURL.setValue(response.appURL);
    this.settingsForm.controls.clientId.setValue(response.clientId);
    this.settingsForm.controls.clientSecret.setValue(response.clientSecret);
    this.settingsForm.controls.expireRequestLogInMinutes.setValue(
      response.expireRequestLogInMinutes,
    );
    this.settingsForm.controls.callbackProtocol.setValue(
      response.callbackProtocol,
    );
  }

  updateSettings() {
    this.settingsService
      .updateSettings(
        this.settingsForm.controls.appURL.value,
        this.settingsForm.controls.clientId.value,
        this.settingsForm.controls.clientSecret.value,
        this.settingsForm.controls.expireRequestLogInMinutes.value,
        this.settingsForm.controls.callbackProtocol.value,
      )
      .subscribe({
        next: response => {
          this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
        },
        error: error => {
          this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
        },
      });
  }

  registerService() {
    this.settingsService.registerService().subscribe({
      next: success => {
        this.snackBar.open(SERVICE_REGISTERED_SUCCESSFULLY, CLOSE, {
          duration: DURATION,
        });
      },
      error: error => {
        this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
      },
    });
  }
}
