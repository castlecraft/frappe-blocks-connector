import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { RequestLogService } from './request-log.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { NEW_ID } from '../../../app/constants/common';

@Component({
  selector: 'app-request-log',
  templateUrl: './request-log.component.html',
  styleUrls: ['./request-log.component.css'],
})
export class RequestLogComponent implements OnInit {
  providerUuid: string;
  userUuid: string;
  creation: string;
  successResponse: string;
  failResponse: string;
  uuid: string;
  model: string;

  requestLogForm = new FormGroup({
    providerUuid: new FormControl(this.providerUuid),
    userUuid: new FormControl(this.userUuid),
    creation: new FormControl(this.creation),
    successResponse: new FormControl(this.successResponse),
    failResponse: new FormControl(this.failResponse),
  });

  constructor(
    private router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly service: RequestLogService,
  ) {
    this.uuid = this.activatedRoute.snapshot.params.id;
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[2];
      });
  }

  ngOnInit() {
    if (this.uuid === NEW_ID) {
      this.uuid = undefined;
    } else {
      this.subscribeRequestLog(this.uuid);
    }
  }

  subscribeRequestLog(uuid: string) {
    this.service.getRequestLog(uuid).subscribe({
      next: response => {
        this.providerUuid = response.providerUuid;
        this.userUuid = response.userUuid;
        this.creation = response.creation;
        this.successResponse = response.successResponse;
        this.failResponse = response.failureResponse;

        this.requestLogForm.controls.providerUuid.setValue(
          response.providerUuid,
        );
        this.requestLogForm.controls.userUuid.setValue(response.userUuid);
        this.requestLogForm.controls.creation.setValue(response.creation);
        this.requestLogForm.controls.successResponse.setValue(
          JSON.stringify(response.successResponse, null, 2),
        );
        this.requestLogForm.controls.failResponse.setValue(
          JSON.stringify(response.failResponse, null, 2),
        );
      },
      error: error => {},
    });
  }
}
