import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrappeConnectorSettingsComponent } from './frappe-connector-settings/frappe-connector-settings.component';
import { FrappeClientComponent } from './frappe-client/frappe-client.component';
import { RequestLogComponent } from './request-log/request-log.component';
import { FrappeConnectorSettingsService } from './frappe-connector-settings/frappe-connector-settings.service';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { FrappeClientService } from './frappe-client/frappe-client.service';
import { RequestLogService } from './request-log/request-log.service';

@NgModule({
  declarations: [
    FrappeConnectorSettingsComponent,
    FrappeClientComponent,
    RequestLogComponent,
  ],
  providers: [
    FrappeConnectorSettingsService,
    FrappeClientService,
    RequestLogService,
  ],
  imports: [CommonModule, SharedImportsModule],
})
export class FrappeConnectorUIModule {}
