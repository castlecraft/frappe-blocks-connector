FROM node:latest
# Copy app
COPY . /home/craft/frappe-connector
WORKDIR /home/craft/
RUN cd frappe-connector \
    && npm install \
    && npm run prestart:prod \
    && rm -fr node_modules \
    && npm install --only=production

FROM node:slim
# Install Dockerize
ENV DOCKERIZE_VERSION v0.6.1
RUN apt-get update \
    && apt-get install -y wget \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && apt-get purge -y wget \
    && apt-get clean

# Setup docker-entrypoint
COPY docker/docker-entrypoint.sh usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

# Add non root user
RUN useradd -ms /bin/bash craft
WORKDIR /home/craft/frappe-connector
COPY --from=0 /home/craft/frappe-connector .

RUN chown -R craft:craft /home/craft

# set project directory
WORKDIR /home/craft/frappe-connector

# Expose port
EXPOSE 3300

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["start"]
