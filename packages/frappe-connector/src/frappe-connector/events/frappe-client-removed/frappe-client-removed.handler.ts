import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { FrappeClientRemovedEvent } from './frappe-client-removed.event';
import { FrappeTokenService } from '../../entities/frappe-token/frappe-token.service';

@EventsHandler(FrappeClientRemovedEvent)
export class FrappeClientRemovedHandler
  implements IEventHandler<FrappeClientRemovedEvent> {
  constructor(private readonly token: FrappeTokenService) {}

  handle(event: FrappeClientRemovedEvent) {
    const { client: provider } = event;
    this.token
      .find({ providerUuid: provider.uuid })
      .then(tokens => {
        for (const token of tokens) {
          token
            .remove()
            .then(success => {})
            .catch(error => {});
        }
      })
      .catch(error => {});

    provider
      .remove()
      .then(success => {})
      .catch(error => {});
  }
}
