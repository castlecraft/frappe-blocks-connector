import { Test } from '@nestjs/testing';
import { CqrsModule, EventBus } from '@nestjs/cqrs';
import { FrappeClientUpdatedHandler } from './frappe-client-updated.handler';
import { FrappeClientUpdatedEvent } from './frappe-client-updated.event';
import { FrappeClient } from '../../entities/frappe-client/frappe-client.entity';

describe('Event: FrappeClientUpdatedHandler', () => {
  let eventBus$: EventBus;
  let eventHandler: FrappeClientUpdatedHandler;
  const mockProvider = new FrappeClient();

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [CqrsModule],
      providers: [
        FrappeClientUpdatedHandler,
        {
          provide: EventBus,
          useFactory: () => jest.fn(),
        },
      ],
    }).compile();

    eventBus$ = module.get<EventBus>(EventBus);
    eventHandler = module.get<FrappeClientUpdatedHandler>(
      FrappeClientUpdatedHandler,
    );
  });

  it('should be defined', () => {
    expect(eventBus$).toBeDefined();
    expect(eventHandler).toBeDefined();
  });

  it('should save OAuth2Provider', async () => {
    eventBus$.publish = jest.fn(() => {});
    mockProvider.save = jest.fn(() => Promise.resolve(mockProvider));
    await eventHandler.handle(new FrappeClientUpdatedEvent(mockProvider));
    expect(mockProvider.save).toHaveBeenCalledTimes(1);
  });
});
