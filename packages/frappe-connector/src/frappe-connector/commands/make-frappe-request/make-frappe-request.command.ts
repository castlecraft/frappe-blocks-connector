import { ICommand } from '@nestjs/cqrs';

export class MakeFrappeRequestCommand implements ICommand {
  constructor(
    public readonly headers: any,
    public readonly requestMethod: string,
    public readonly clientUuid: string,
    public readonly params: any,
    public readonly query: any,
    public readonly payload: any,
    public readonly userUuid: string,
  ) {}
}
