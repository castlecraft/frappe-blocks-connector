import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { MakeFrappeRequestCommand } from './make-frappe-request.command';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';

@CommandHandler(MakeFrappeRequestCommand)
export class MakeFrappeRequestHandler
  implements ICommandHandler<MakeFrappeRequestCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: FrappeClientAggregateService,
  ) {}

  async execute(command: MakeFrappeRequestCommand) {
    const {
      headers,
      requestMethod,
      clientUuid,
      params,
      query,
      payload,
      userUuid,
    } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);

    const requestLog = await aggregate.makeRequest(
      headers,
      requestMethod,
      clientUuid,
      params,
      query,
      payload,
      userUuid,
    );

    return requestLog;
  }
}
