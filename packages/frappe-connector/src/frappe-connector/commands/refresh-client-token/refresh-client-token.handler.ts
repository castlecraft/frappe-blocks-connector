import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RefreshClientTokenCommand } from './refresh-client-token.command';
import { FrappeClientAggregateService } from '../../aggregates/frappe-client-aggregate/frappe-client-aggregate.service';
import { BadRequestException } from '@nestjs/common';
import { INVALID_FRAPPE_TOKEN } from '../../../constants/messages';

@CommandHandler(RefreshClientTokenCommand)
export class RefreshClientTokenHandler
  implements ICommandHandler<RefreshClientTokenCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: FrappeClientAggregateService,
  ) {}

  async execute(command: RefreshClientTokenCommand) {
    const { providerUuid, userUuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const token = await this.manager.findOneToken(providerUuid, userUuid);
    if (!token) throw new BadRequestException(INVALID_FRAPPE_TOKEN);
    await aggregate.refreshToken(token);
  }
}
