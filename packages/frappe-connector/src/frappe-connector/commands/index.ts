import { AddFrappeClientHandler } from './add-frappe-client/add-frappe-client.handler';
import { RemoveFrappeClientHandler } from './remove-frappe-client/remove-frappe-client.handler';
import { UpdateFrappeClientHandler } from './update-frappe-client/update-frappe-client.handler';
import { MakeFrappeRequestHandler } from './make-frappe-request/make-frappe-request.handler';
import { RefreshClientTokenHandler } from './refresh-client-token/refresh-client-token.handler';

export const FrappeCommandHandlers = [
  AddFrappeClientHandler,
  RemoveFrappeClientHandler,
  UpdateFrappeClientHandler,
  MakeFrappeRequestHandler,
  RefreshClientTokenHandler,
];
