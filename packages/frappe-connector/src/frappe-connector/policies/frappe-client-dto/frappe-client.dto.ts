import { IsString, IsUrl, IsNotEmpty, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FrappeClientDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description:
      'Human readable name of client_id registered on frappe instance',
    type: 'string',
    required: true,
  })
  name: string;

  @IsUrl()
  @ApiProperty({
    description: 'Base URL of frappe instance',
    type: 'string',
    required: true,
  })
  authServerURL: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'registered client_id on frappe instance',
    type: 'string',
    required: true,
  })
  clientId: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'client_secret of registered client on frappe instance',
    type: 'string',
    required: true,
  })
  clientSecret: string;

  @IsUrl()
  @ApiProperty({
    description: 'OpenID Connect User Info endpoint',
    type: 'string',
    required: true,
  })
  profileURL: string;

  @IsUrl()
  @ApiProperty({
    description: 'OAuth 2 token endpoint of frappe instance',
    type: 'string',
    required: true,
  })
  tokenURL: string;

  @IsUrl()
  @ApiProperty({
    description: 'OAuth 2 authorization endpoint of frappe instance',
    type: 'string',
    required: true,
  })
  authorizationURL: string;

  @IsUrl()
  @ApiProperty({
    description: 'OAuth 2 token revocation endpoint of frappe instance',
    type: 'string',
    required: true,
  })
  revocationURL: string;

  @IsArray()
  @IsNotEmpty({ each: true })
  @ApiProperty({
    description:
      'String Array of scope allowed for client registered on frappe instance',
    isArray: true,
    type: 'string',
    required: true,
  })
  scope: string[];
}
