import { Entity, ObjectIdColumn, ObjectID, Column, BaseEntity } from 'typeorm';

@Entity()
export class RequestLog extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  providerUuid: string;

  @Column()
  userUuid: string;

  @Column()
  creation: Date;

  @Column()
  expiration: Date;

  @Column()
  successResponse: any;

  @Column()
  failResponse: any;
}
