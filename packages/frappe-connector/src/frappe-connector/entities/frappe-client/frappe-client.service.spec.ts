import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { FrappeClient } from './frappe-client.entity';
import { FrappeClientService } from './frappe-client.service';

describe('FrappeClientService', () => {
  let service: FrappeClientService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FrappeClientService,
        {
          provide: getRepositoryToken(FrappeClient),
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<FrappeClientService>(FrappeClientService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
