import {
  Controller,
  Param,
  UseGuards,
  Req,
  Query,
  Post,
  Get,
  Res,
  Body,
  UsePipes,
  ValidationPipe,
  ForbiddenException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { ApiBearerAuth } from '@nestjs/swagger';
import { FrappeTokenManagerService } from '../../aggregates/frappe-token-manager/frappe-token-manager.service';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { FrappeClientDto } from '../../policies/frappe-client-dto/frappe-client.dto';
import { UpdateFrappeClientDto } from '../../policies/update-frappe-client-dto/update-frappe-client.dto';
import { AddFrappeClientCommand } from '../../commands/add-frappe-client/add-frappe-client.command';
import { RemoveFrappeClientCommand } from '../../commands/remove-frappe-client/remove-frappe-client.command';
import { UpdateFrappeClientCommand } from '../../commands/update-frappe-client/update-frappe-client.command';
import { VerifyClientConnectionQuery } from '../../queries/verify-client-connection/verify-client-connection.query';
import { ListFrappeClientQuery } from '../../queries/list-frappe-client/list-frappe-client.query';
import { RetrieveFrappeClientQuery } from '../../queries/retrieve-frappe-client/retrieve-frappe-client.query';
import { RefreshClientTokenCommand } from '../../commands/refresh-client-token/refresh-client-token.command';
import { INVALID_CLIENT } from '../../../constants/messages';
import { AuthServerVerificationGuard } from '../../../auth/guards/authserver-verification.guard';

@Controller('frappe')
export class FrappeController {
  constructor(
    private readonly tokenManager: FrappeTokenManagerService,
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get('callback')
  callback(@Query('code') code, @Query('state') state, @Res() res) {
    this.tokenManager.processCode(code, state, res);
  }

  @ApiBearerAuth()
  @Post('v1/connect_client_for_user/:uuid')
  @UseGuards(TokenGuard)
  connectClientForUser(
    @Param('uuid') uuid,
    @Query('redirect') redirect,
    @Req() req,
  ) {
    return this.tokenManager.connectClientForUser(uuid, redirect, req);
  }

  @ApiBearerAuth()
  @Post('v1/connect_client_for_user_by_password/:uuid')
  @UseGuards(TokenGuard)
  connectClientForUserByPassword(
    @Param('uuid') uuid,
    @Body('username') username: string,
    @Body('password') password: string,
    @Req() req,
  ) {
    return this.tokenManager.connectClientForUserByPassword(
      uuid,
      username,
      password,
      req.token.sub,
    );
  }

  @ApiBearerAuth()
  @Post('v1/connect_client')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  async connectClient(@Body() payload: FrappeClientDto) {
    return await this.commandBus.execute(new AddFrappeClientCommand(payload));
  }

  @ApiBearerAuth()
  @Post('v1/connect_client_with_credentials')
  @Roles(ADMINISTRATOR)
  @UseGuards(AuthServerVerificationGuard)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  async connectClientWithCredentials(@Body() payload: FrappeClientDto) {
    return await this.commandBus.execute(new AddFrappeClientCommand(payload));
  }

  @ApiBearerAuth()
  @Post('v1/connect_client_with_token')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  async connectClientWithToken(@Body() payload: FrappeClientDto, @Req() req) {
    if (!req.token.trustedClient || req.token.sub) {
      throw new ForbiddenException(INVALID_CLIENT);
    }
    return await this.commandBus.execute(new AddFrappeClientCommand(payload));
  }

  @ApiBearerAuth()
  @Post('v1/disconnect_client/:uuid')
  @Roles(ADMINISTRATOR)
  @UseGuards(TokenGuard, RoleGuard)
  async disconnectClient(@Param('uuid') uuid, @Req() req) {
    const user = req.token.sub;
    return await this.commandBus.execute(
      new RemoveFrappeClientCommand(uuid, user),
    );
  }

  @ApiBearerAuth()
  @Post('v1/update_client/:uuid')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ transform: true, whitelist: true }))
  @UseGuards(TokenGuard, RoleGuard)
  async updateClient(
    @Param('uuid') uuid,
    @Body() payload: UpdateFrappeClientDto,
  ) {
    return await this.commandBus.execute(
      new UpdateFrappeClientCommand(uuid, payload),
    );
  }

  @ApiBearerAuth()
  @Get('v1/verify_client_connection/:providerUuid')
  @UseGuards(TokenGuard)
  async verifyClientConnection(
    @Param('providerUuid') providerUuid,
    @Req() req,
  ) {
    const userUuid = req.token.sub;
    return await this.queryBus.execute(
      new VerifyClientConnectionQuery(userUuid, providerUuid),
    );
  }

  @ApiBearerAuth()
  @Get('v1/list')
  @UseGuards(TokenGuard)
  async getClientList(
    @Query('offset') offset: number,
    @Query('limit') limit: number,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
  ) {
    return await this.queryBus.execute(
      new ListFrappeClientQuery(offset, limit, search, sort),
    );
  }

  @ApiBearerAuth()
  @Get('v1/get/:clientUuid')
  async getFrappeClient(@Param('clientUuid') clientUuid) {
    return await this.queryBus.execute(
      new RetrieveFrappeClientQuery(clientUuid),
    );
  }

  @ApiBearerAuth()
  @Post('v1/refresh_token_for_client/:clientUuid')
  @UseGuards(TokenGuard)
  async refreshTokenForClient(@Param('clientUuid') clientUuid, @Req() req) {
    const userUuid = req.token.sub;
    await this.commandBus.execute(
      new RefreshClientTokenCommand(clientUuid, userUuid),
    );
  }
}
