import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRequestLogQuery } from './retrieve-request-log.query';
import { RequestLogManagerService } from '../../aggregates/request-log-manager/request-log-manager.service';

@QueryHandler(RetrieveRequestLogQuery)
export class RetrieveRequestLogHandler
  implements IQueryHandler<RetrieveRequestLogQuery> {
  constructor(private manager: RequestLogManagerService) {}

  async execute(query: RetrieveRequestLogQuery) {
    const { uuid } = query;
    return await this.manager.retrieveProvider(uuid);
  }
}
