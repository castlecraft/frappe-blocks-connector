import { Injectable, NotFoundException } from '@nestjs/common';
import { RequestLogService } from '../../entities/request-log/request-log.service';

@Injectable()
export class RequestLogManagerService {
  constructor(private readonly requestLogService: RequestLogService) {}

  async list(offset: number, limit: number, search?: string, sort?: string) {
    offset = Number(offset);
    limit = Number(limit);
    return this.requestLogService.list(offset, limit);
  }

  async retrieveProvider(uuid: string) {
    const provider = await this.requestLogService.findOne({ uuid });
    if (!provider) throw new NotFoundException({ uuid });
    return provider;
  }

  async getUserList(offset, limit, uuid, search, sort) {
    const provider = await this.requestLogService.find({
      where: { userUuid: uuid },
      skip: Number(offset),
      take: Number(limit),
    });
    return {
      docs: provider,
      offset,
      length: await this.requestLogService.count({ userUuid: uuid }),
    };
  }
}
