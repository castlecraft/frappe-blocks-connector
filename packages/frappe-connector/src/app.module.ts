import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TerminusModule } from '@nestjs/terminus';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { connectTypeorm } from './constants/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { FrappeConnectorModule } from './frappe-connector/frappe-connector.module';
import { TerminusOptionsService } from './system-settings/aggregates/terminus-options/terminus-options.service';
import { ConfigService } from './config/config.service';
import { EventStoreModule } from './event-store/event-store.module';
import { FrappeCommandModule } from './frappe-command/frappe-command.module';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: connectTypeorm,
      inject: [ConfigService],
    }),
    TerminusModule.forRootAsync({ useClass: TerminusOptionsService }),
    ConfigModule,
    AuthModule,
    SystemSettingsModule,
    FrappeConnectorModule,
    EventStoreModule,
    FrappeCommandModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
