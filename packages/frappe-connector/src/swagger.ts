import { INestApplication } from '@nestjs/common';
import { readFileSync } from 'fs';
import { join } from 'path';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { APP_NAME, SWAGGER_ROUTE } from './constants/app-strings';
import { AuthModule } from './auth/auth.module';
import { FrappeConnectorModule } from './frappe-connector/frappe-connector.module';
import { ConfigModule } from './config/config.module';
import { EventStoreModule } from './event-store/event-store.module';
import { AuthEntitiesModule } from './auth/entities/auth-entities.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';

export function setupSwagger(app: INestApplication) {
  const packageJson = JSON.parse(
    readFileSync(join(process.cwd(), 'package.json'), 'utf-8'),
  );
  const version = packageJson.version;
  const description = packageJson.description;
  const options = new DocumentBuilder()
    .setTitle(APP_NAME)
    .setDescription(description)
    .setVersion(version)
    .build();
  const document = SwaggerModule.createDocument(app, options, {
    include: [
      AuthModule,
      AuthEntitiesModule,
      FrappeConnectorModule,
      ConfigModule,
      EventStoreModule,
      SystemSettingsModule,
    ],
  });
  SwaggerModule.setup(SWAGGER_ROUTE, app, document);
}
