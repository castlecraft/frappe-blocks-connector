import { Module, HttpModule } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CommandController } from './controllers/command/command.controller';

@Module({
  imports: [HttpModule, CqrsModule],
  controllers: [CommandController],
})
export class FrappeCommandModule {}
