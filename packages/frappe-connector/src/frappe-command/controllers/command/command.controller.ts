import {
  Controller,
  All,
  UseGuards,
  Param,
  Query,
  Body,
  Req,
  ForbiddenException,
  Headers,
} from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { MakeFrappeRequestCommand } from '../../../frappe-connector/commands/make-frappe-request/make-frappe-request.command';
import { INVALID_CLIENT } from '../../../constants/messages';

@Controller('frappe')
export class CommandController {
  constructor(private readonly commandBus: CommandBus) {}
  @All('v1/command/:clientUuid/*')
  @UseGuards(TokenGuard)
  async makeRequest(
    @Param('clientUuid') clientUuid,
    @Param() params,
    @Query() query,
    @Body() payload,
    @Req() req,
    @Headers() headers,
  ) {
    const userUuid = req.token.sub;
    return await this.commandBus.execute(
      new MakeFrappeRequestCommand(
        headers,
        req.method,
        clientUuid,
        params,
        query,
        payload,
        userUuid,
      ),
    );
  }

  @All('v1/command_as_trusted_client/:clientUuid/:userUuid/*')
  @UseGuards(TokenGuard)
  async makeRequestAsTrustedClient(
    @Param('clientUuid') clientUuid,
    @Param('userUuid') userUuid,
    @Param() params,
    @Query() query,
    @Body() payload,
    @Req() req,
    @Headers() headers,
  ) {
    if (!req.token.trustedClient || req.token.sub) {
      throw new ForbiddenException(INVALID_CLIENT);
    }

    return await this.commandBus.execute(
      new MakeFrappeRequestCommand(
        headers,
        req.method,
        clientUuid,
        params,
        query,
        payload,
        userUuid,
      ),
    );
  }
}
