### Register Service

- User with role `administrator` can make HTTP POST request to `/settings/v1/register_service` with header `Authorization: Bearer {access_token}` replace `{access_token}` with user's `access_token`

- Or visit `/settings` and set it from Web UI.

This will register the frappe-connector on infrastructure-console and make it discoverable via REST API.
Any app can dynamically get URL of this service using `/service` api of infrastructure-console.
