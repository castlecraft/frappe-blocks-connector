### Initialize Service

To setup the service make a HTTP POST request to `/setup` endpoint with following body and headers

- appURL - URL of the frappe connector
- authServerURL - URL of authorization server
- clientId - `client_id` registered on Authorization server
- clientSecret - Registered client's `client_secret`

```sh
curl -X POST \
    --header "Content-Type: application/x-www-form-urlencoded" \
    -d "appURL=http://decaf.example.com&
        authServerURL=https://accounts.example.com&
        clientId=dd1c1952-d717-4872-bc6a-416af691b49c&
        clientSecret=YTgxY35PPFnmBatnyz858zJrCQT3sbP8DC68gZanmAeemnKfGXhHUXf3Q3b5CJMH"
```

This will connect the frappe-connector service and enable the frappe-connector-ui on `appURL`
