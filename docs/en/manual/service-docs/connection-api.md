### Prerequisites

- Social login added in your erpnext or frappe app. (Register a client on Authorization Server)
- Optionally auto login on erpnext for seamless experience.
- Register the Service as administrator. This is to make it discoverable for your apps dynamically.
- Search for frappe-connector on your `infrastructure-console` .e.g. `/service/v1/list?type=frappe-connector`
- It will return `docs` array of services. Each service will have `serviceURL` which is `appURL` of frappe-connector registered.
- This `serviceURL` can be used to access endpoints on the service.
- `uuid` of `FrappeClient` registered on frappe-connector is necessary for following commands

### Verify Connection

Make HTTP GET request with header `Authorization: Bearer {access_token}` to `/frappe/v1/verify_client_connection/{uuid_of_frappe_client}`

Note:
 - replace `{access_token}` with user's `access_token`
 - replace `{uuid_of_frappe_client}` with uuid of Frappe Client to access the frappe/erpnext instance

response will be a boolean to signify whether the user has frappe's token on frappe-connector.

example:

```json
{
    "isConnected": false
}
```

### Connect User to Client Instance

Make HTTP POST request with header `Authorization: Bearer {access_token}` to `/frappe/v1/connect_client_for_user/{uuid_of_frappe_client}?redirect={encoded_url}`


Note:
 - replace `{access_token}` with user's `access_token`
 - replace `{uuid_of_frappe_client}` with uuid of Frappe Client to access the frappe/erpnext instance
 - Optionally, add a query parameter for `redirect` with url encoded string. e.g. `?redirect=https%3A%2F%2Fwww.mntechnique.com`

response will be a redirect url. Redirect your browser or user to this url to initialize the connection and authorization flow.

example:

```json
{
    "redirect": "<authorization_url>"
}
```

After successful login (optionally seamless), user is connected and can make API calls to `command` api.

### Connection Details

- It will store expiration time on token and refresh frappe's token 20 minutes before it expires
- The refresh happens only if there is a call to use the token in frappe command api of frappe-connector
- It will delete token if revoked from frappe end.
- If revoked token is identified only on some call. Apps must check for this on every RequestLog
