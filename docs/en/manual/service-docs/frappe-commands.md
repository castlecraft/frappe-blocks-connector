### Make request to Frappe / ERPNext instances

Make HTTP Request with header `Authorization: Bearer {access_token}` to `/frappe/command/{uuid_of_frappe_client}/{frappe_request}`

Any HTTP Request Method that is allowed on frappe server can be used here.

Note:
 - replace `{access_token}` with user's `access_token`
 - replace `{uuid_of_frappe_client}` with uuid of Frappe Client to access the frappe/erpnext instance
 - replace `{frappe_request}` with `/api/method/path.to.rpc.call` or `/api/resource/...` the request made to frappe instance
 - Pass the appropriate headers as required by frappe ReST API.
