### Prerequisites

- Kubernetes cluster is basic requirement. (Docker images for docker-compose/docker swarm are possible. They are not covered in documentation)
- `kubectl` and `helm` clients installed
- `mongo` client tools installed

### Prepare cluster

Refer building blocks [documentation](http://castlecraft.gitlab.io/building-blocks)

MongoDB helm chart is installed in global-mongo namespace (refer mongodb helm chart docs)

Connect to mongo locally to created databases.

```sh
# export root password
export MONGODB_ROOT_PASSWORD=$(kubectl get secret --namespace mongodb global-mongodb -o jsonpath="{.data.mongodb-root-password}" | base64 --decode)

# port-forward database from cluster locally
kubectl port-forward --namespace mongodb svc/global-mongodb 27017:27017 &

# connect database using mongo client
mongo --host 127.0.0.1 --authenticationDatabase admin -p $MONGODB_ROOT_PASSWORD -u root
```

Create database using mongo shell, we need a db for frappe-connector. Following is example command for creating a database.

```
mongo frappe-connector \
    --host localhost \
    --port 27017 \
    -u root \
    -p $MONGODB_ROOT_PASSWORD \
    --authenticationDatabase admin \
    --eval "db.createUser({user: 'frappe-connector', pwd: 'secret', roles:[{role:'dbOwner', db: 'frappe-connector'}]});"
```

Note: In case of managed db, you need to provide hostnames as env variables. No need to setup databases on cluster. Refer mongodb docs to create databases with user and password access.

### Clone helm charts repository

```sh
git clone https://github.com/castlecraft/helm-charts
```

### Set values.yaml

Use any editor and set the environment variables and values for the created/managed databases.

```sh
code helm-charts/frappe-blocks-connector/values.yaml
```

### Install Building Blocks

refer README https://github.com/castlecraft/helm-charts/tree/master/frappe-blocks-connector
Use the modified values.yaml instead of the one on repo.
