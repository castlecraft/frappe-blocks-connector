# About

This is a service to connect frappe instances to building blocks infrastructure.
It uses OAuth 2.0 authorization server available on frappe instances.
Add a `Frappe Client` and start using api to connect ERPNext or frappe apps.
Each request is logged for 24 hours by default. It can be processed within that time.

Frappe Connector acts as push gateway for microservices.

Use Frappe's Webhooks from frappe / erpnext instances to push data back to any microservice.

![cluster](assets/frappe-blocks-connector.png)

### Development

Start development environment. Refer [section](/development/README.md)

DO NOT setup local development servers to develop apps based on this service.
Host a common setup under a kubernetes namespace for developers.
