# Summary

* [About](README.md)

* For Development
    * [Setup](/development/README.md)
    * [Visual Studio Code](/development/vscode.md)
    * [Codebase](/development/codebase.md)
    * [Dependencies](/development/dependencies.md)
    * [Configuration](development/config.md)
    * [Backing Services](/development/services.md)
    * [Pipeline](/development/pipeline.md)
    * [Port Binding](/development/port-binding.md)

* For Production
    * [Production Deployment](/production/README.md)

* Service Documentation
    * [Initialize Service](/service-docs/README.md)
    * [Administrator Setup](/service-docs/admin-setup.md)
    * [Connection API](/service-docs/connection-api.md)
    * [Frappe Commands](/service-docs/frappe-commands.md)
