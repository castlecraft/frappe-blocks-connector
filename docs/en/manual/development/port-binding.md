# Port binding

- Frappe Connector exposes port `3300`
- Frappe Connector UI serves on `4040` during development, production nginx serves on `8080`
