# Dependencies

### NestJS

It is used to build backend. Tests are written as per the documentation provided by NestJS.

### Angular

It is used to build frontend. Tests are written as per the documentation provided by Angular.

### TypeScript, Formatting and Linting

All the code is built in TypeScript. Prettier (`npm run format`) does the formatting for projects. ts-lint (`npm run lint --fix`) checks and fixes the linting. This is checked as part of CI pipeline. Code format and linting errors will result in CI failure. `ts-lint` and `prettier` is used.

### Gitbook

Gitbook is used to generate documentation. Gitbook should be installed globally. Following command executed from repo root will result in building of gitbook

```sh
gitbook build docs public
```

### Lerna

Lerna is used to publish releases on git and npm. Lerna is also used to bootstrap development environment.

App specific dependencies will be discussed in detail under app's section.
