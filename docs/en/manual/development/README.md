# Development Installation

### Setup hosts file

add `127.0.0.1 *.localhost` in `/etc/hosts` file or hosts file of your operating system.

### Install Prerequisites

- Docker (to run backing services containers for redis and mongo)
- Docker compose (easy bootstrap of development setup)
- NVM / NodeJS (use nvm to manage different node versions required)
- VS Code (Editor and NodeJS/TypeScript IDE)

### Install NodeJS global commands

```
# use nvm for better control and secure node environments for users.
# DO NOT USE sudo or root privileges
npm i lerna @angular/cli @nestjs/cli -g
```

### Clone Repository and set working directory

```sh
git clone https://gitlab.com/castlecraft/frappe-blocks-connector
cd frappe-blocks-connector
```

### Bootstrap NodeJS package dependencies

```sh
rm -fr node_modules && npm i && lerna clean -y && lerna bootstrap
```

### Setup Environment Variables

Copy `env-example` to `.env` file:

```sh
cp docker/env-example .env
cp packages/frappe-connector/env-example packages/frappe-connector/.env
```

### Start Backing Services

```
docker-compose --project-name fbc -f docker/backing-services/docker-compose.yml up -d
```

### Start apps and frontends

Start Development backend and frontend using following commands

```
# for packages/frappe-connector,
# execute following command from the app package root
npm run start:debug

# for frontend/frappe-connector-ui,
# execute following command from the frontend package root
npm start
```

or use VS Code Setup, refer [example](/development/vscode.md)

### Commands for testing

```
# NestJS and Angular unit tests
lerna run test

# NestJS e2e/integration
lerna run test:e2e

# Angular e2e
lerna --concurrency 1 run e2e

# Check format
lerna run format:check

# Check Linting
lerna run lint
```

### Commands to format code and lint fixes

```
# To execute from project root
lerna run format && lerna run lint -- --fix

# OR execute from app or frontend package root
npm run format && npm run lint -- --fix
```

# ReST API Swagger Docs

ReST API Docs app can be accessed at `/api-docs` and `/api-docs-json`
