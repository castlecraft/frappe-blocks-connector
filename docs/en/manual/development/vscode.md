# Visual Studio Code IDE

To improve development speed while using VS Code you can setup configuration similar to this.
This configuration works on linux machine. You need to make changes as per your OS.

### launch.json

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "frappe-connector-ui",
            "url": "http://fcon.localhost:4040",
            "runtimeExecutable": "/usr/bin/chromium",
            "runtimeArgs": ["--remote-debugging-port=9222", "--user-data-dir=/tmp/bb/fcon-ui"],
            "webRoot": "${workspaceFolder}/frontend/frappe-connector-ui"
        }
    ]
}
```

### settings.json

```json
{
    "debug.node.autoAttach": "on"
}
```

### tasks.json

```json
{
    "version": "2.0.0",
    "tasks": [
        {
            "type": "npm",
            "script": "start:debug",
            "path": "packages/frappe-connector/",
            "problemMatcher": []
        },
        {
            "type": "npm",
            "script": "start",
            "path": "frontend/frappe-connector-ui/",
            "problemMatcher": []
        }
    ]
}
```
