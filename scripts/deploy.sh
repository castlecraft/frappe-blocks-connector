#!/bin/bash

cd /tmp

# Clone building blocks configuration
git clone https://github.com/castlecraft/helm-charts

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_FC=$(helm ls -q frappe-connector-staging --tiller-namespace staging)
if [ "$CHECK_FC" = "frappe-connector-staging" ]
then
    echo "Updating existing frappe-connector-staging . . ."
    helm upgrade frappe-connector-staging \
        --tiller-namespace staging \
        --namespace staging \
        --reuse-values \
        helm-charts/frappe-blocks-connector
fi
